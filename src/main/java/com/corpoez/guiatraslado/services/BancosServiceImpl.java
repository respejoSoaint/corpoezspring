package com.corpoez.guiatraslado.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.corpoez.guiatraslado.models.Bancos;
import com.corpoez.guiatraslado.repository.IBancosDAO;

@Service
public class BancosServiceImpl implements IBancosService {
	
	@Autowired
	private IBancosDAO bancosDAO;

	@Override
	@Transactional(readOnly = true)
	public List<Bancos> findAll() {
		return (List<Bancos>) bancosDAO.findAll();
	}

}
