package com.corpoez.guiatraslado.services;

import java.util.List;

import com.corpoez.guiatraslado.models.Bancos;

public interface IBancosService {
	
	public List<Bancos> findAll();

}
