package com.corpoez.guiatraslado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuiaTrasladoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuiaTrasladoApplication.class, args);
	}

}
