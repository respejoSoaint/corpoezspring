package com.corpoez.guiatraslado.repository;

import org.springframework.data.repository.CrudRepository;

import com.corpoez.guiatraslado.models.Bancos;

public interface IBancosDAO extends CrudRepository<Bancos, Long> {

}
