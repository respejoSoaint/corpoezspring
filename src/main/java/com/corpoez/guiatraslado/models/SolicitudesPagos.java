package com.corpoez.guiatraslado.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "solicitudesPagos")
public class SolicitudesPagos {
	
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSolPago;
	private int idSolicitud;
	private int idBanco;
	private int idTipoMoneda;
	private String nombreArchivoPago;
	private String rutaArchivoPago;
	private String numReferencia;
	private String montoPago;
	private String createdAt;
	private int createdUser;
	public Long getIdSolPago() {
		return idSolPago;
	}
	public void setIdSolPago(Long idSolPago) {
		this.idSolPago = idSolPago;
	}
	public int getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(int idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public int getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(int idBanco) {
		this.idBanco = idBanco;
	}
	public int getIdTipoMoneda() {
		return idTipoMoneda;
	}
	public void setIdTipoMoneda(int idTipoMoneda) {
		this.idTipoMoneda = idTipoMoneda;
	}
	public String getNombreArchivoPago() {
		return nombreArchivoPago;
	}
	public void setNombreArchivoPago(String nombreArchivoPago) {
		this.nombreArchivoPago = nombreArchivoPago;
	}
	public String getRutaArchivoPago() {
		return rutaArchivoPago;
	}
	public void setRutaArchivoPago(String rutaArchivoPago) {
		this.rutaArchivoPago = rutaArchivoPago;
	}
	public String getNumReferencia() {
		return numReferencia;
	}
	public void setNumReferencia(String numReferencia) {
		this.numReferencia = numReferencia;
	}
	public String getMontoPago() {
		return montoPago;
	}
	public void setMontoPago(String montoPago) {
		this.montoPago = montoPago;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public int getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(int createdUser) {
		this.createdUser = createdUser;
	}
	
	@Override
	public String toString() {
		return "SolicitudesPagos [idSolPago=" + idSolPago + ", idSolicitud=" + idSolicitud + ", idBanco=" + idBanco
				+ ", idTipoMoneda=" + idTipoMoneda + ", nombreArchivoPago=" + nombreArchivoPago + ", rutaArchivoPago="
				+ rutaArchivoPago + ", numReferencia=" + numReferencia + ", montoPago=" + montoPago + ", createdAt="
				+ createdAt + ", createdUser=" + createdUser + "]";
	}
	
	
	

}
