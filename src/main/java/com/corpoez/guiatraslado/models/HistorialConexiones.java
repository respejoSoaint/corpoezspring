package com.corpoez.guiatraslado.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="historialConexiones")
public class HistorialConexiones {
	
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idHisto;
	private int idUsaurio;
	private String fechaConexion;
	private String ipConexion;
	private String macAdressConexion;
	public Long getIdHisto() {
		return idHisto;
	}
	public void setIdHisto(Long idHisto) {
		this.idHisto = idHisto;
	}
	public int getIdUsaurio() {
		return idUsaurio;
	}
	public void setIdUsaurio(int idUsaurio) {
		this.idUsaurio = idUsaurio;
	}
	public String getFechaConexion() {
		return fechaConexion;
	}
	public void setFechaConexion(String fechaConexion) {
		this.fechaConexion = fechaConexion;
	}
	public String getIpConexion() {
		return ipConexion;
	}
	public void setIpConexion(String ipConexion) {
		this.ipConexion = ipConexion;
	}
	public String getMacAdressConexion() {
		return macAdressConexion;
	}
	public void setMacAdressConexion(String macAdressConexion) {
		this.macAdressConexion = macAdressConexion;
	}
	
	@Override
	public String toString() {
		return "HistorialConexiones [idHisto=" + idHisto + ", idUsaurio=" + idUsaurio + ", fechaConexion="
				+ fechaConexion + ", ipConexion=" + ipConexion + ", macAdressConexion=" + macAdressConexion + "]";
	}
		
	
}
