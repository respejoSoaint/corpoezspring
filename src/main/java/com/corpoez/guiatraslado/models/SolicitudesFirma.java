package com.corpoez.guiatraslado.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "solicitudesFirma")
public class SolicitudesFirma {

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSolFirma;
	private int idSolicitud;
	private String codigoValidacion;
	private String fechaEmision;
	private String codigoQr;
	private String nombreArchivoFirma;
	private String rutaArchivoFirma;
	private String createdAt;
	private int CreatedUser;
	public Long getIdSolFirma() {
		return idSolFirma;
	}
	public void setIdSolFirma(Long idSolFirma) {
		this.idSolFirma = idSolFirma;
	}
	public int getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(int idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public String getCodigoValidacion() {
		return codigoValidacion;
	}
	public void setCodigoValidacion(String codigoValidacion) {
		this.codigoValidacion = codigoValidacion;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getCodigoQr() {
		return codigoQr;
	}
	public void setCodigoQr(String codigoQr) {
		this.codigoQr = codigoQr;
	}
	public String getNombreArchivoFirma() {
		return nombreArchivoFirma;
	}
	public void setNombreArchivoFirma(String nombreArchivoFirma) {
		this.nombreArchivoFirma = nombreArchivoFirma;
	}
	public String getRutaArchivoFirma() {
		return rutaArchivoFirma;
	}
	public void setRutaArchivoFirma(String rutaArchivoFirma) {
		this.rutaArchivoFirma = rutaArchivoFirma;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public int getCreatedUser() {
		return CreatedUser;
	}
	public void setCreatedUser(int createdUser) {
		CreatedUser = createdUser;
	}
	
	@Override
	public String toString() {
		return "SolicitudesFirma [idSolFirma=" + idSolFirma + ", idSolicitud=" + idSolicitud + ", codigoValidacion="
				+ codigoValidacion + ", fechaEmision=" + fechaEmision + ", codigoQr=" + codigoQr
				+ ", nombreArchivoFirma=" + nombreArchivoFirma + ", rutaArchivoFirma=" + rutaArchivoFirma
				+ ", createdAt=" + createdAt + ", CreatedUser=" + CreatedUser + "]";
	}
	
	
	
	
}
