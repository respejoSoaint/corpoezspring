package com.corpoez.guiatraslado.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "solicitudes")
public class Solicitudes {
	
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSolicitud;
	private String numeroSolicitud;
	private String estatusSolicitud;
	private String origen;
	private String destino;
	private String pesoCarga;
	private String createdAt;
	private String updatedAt;
	private int userCreated;
	private int userUpdated;
	private int idChofer;
	private int idVehiculo;
	private int idTipoCarga;
	private int idUsuario;
	
	public Long getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}
	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}
	public String getEstatusSolicitud() {
		return estatusSolicitud;
	}
	public void setEstatusSolicitud(String estatusSolicitud) {
		this.estatusSolicitud = estatusSolicitud;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getPesoCarga() {
		return pesoCarga;
	}
	public void setPesoCarga(String pesoCarga) {
		this.pesoCarga = pesoCarga;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public int getUserCreated() {
		return userCreated;
	}
	public void setUserCreated(int userCreated) {
		this.userCreated = userCreated;
	}
	public int getUserUpdated() {
		return userUpdated;
	}
	public void setUserUpdated(int userUpdated) {
		this.userUpdated = userUpdated;
	}
	public int getIdChofer() {
		return idChofer;
	}
	public void setIdChofer(int idChofer) {
		this.idChofer = idChofer;
	}
	public int getIdVehiculo() {
		return idVehiculo;
	}
	public void setIdVehiculo(int idVehiculo) {
		this.idVehiculo = idVehiculo;
	}
	public int getIdTipoCarga() {
		return idTipoCarga;
	}
	public void setIdTipoCarga(int idTipoCarga) {
		this.idTipoCarga = idTipoCarga;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	@Override
	public String toString() {
		return "Solicitudes [idSolicitud=" + idSolicitud + ", numeroSolicitud=" + numeroSolicitud
				+ ", estatusSolicitud=" + estatusSolicitud + ", origen=" + origen + ", destino=" + destino
				+ ", pesoCarga=" + pesoCarga + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt
				+ ", userCreated=" + userCreated + ", userUpdated=" + userUpdated + ", idChofer=" + idChofer
				+ ", idVehiculo=" + idVehiculo + ", idTipoCarga=" + idTipoCarga + ", idUsuario=" + idUsuario + "]";
	}
	
	
	

}
