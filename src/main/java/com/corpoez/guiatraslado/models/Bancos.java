package com.corpoez.guiatraslado.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "bancos")
public class Bancos implements Serializable {

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idBanco;
	@Column(nullable = false)
	private String nombreBanco;
	
	private String descripcion;
	@Column(nullable = false)
	private String codigoBanco;
	@Column(nullable = false)
	private String numeroCuenta;
	@Column(nullable = false)
	private String tipoCuenta;
	@Column(nullable = false)
	private String titularCuenta;
	private String correoTitular;
	private String telefonoTitular;

	@Temporal(TemporalType.DATE)
	private Date createdAt;

	@Temporal(TemporalType.DATE)
	private Date updatedAt;

	public Long getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(Long idBanco) {
		this.idBanco = idBanco;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigoBanco() {
		return codigoBanco;
	}

	public void setCodigoBanco(String codigoBanco) {
		this.codigoBanco = codigoBanco;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getTitularCuenta() {
		return titularCuenta;
	}

	public void setTitularCuenta(String titularCuenta) {
		this.titularCuenta = titularCuenta;
	}

	public String getCorreoTitular() {
		return correoTitular;
	}

	public void setCorreoTitular(String correoTitular) {
		this.correoTitular = correoTitular;
	}

	public String getTelefonoTitular() {
		return telefonoTitular;
	}

	public void setTelefonoTitular(String telefonoTitular) {
		this.telefonoTitular = telefonoTitular;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public String toString() {
		return "Bancos [idBanco=" + idBanco + ", nombreBanco=" + nombreBanco + ", descripcion=" + descripcion
				+ ", codigoBanco=" + codigoBanco + ", numeroCuenta=" + numeroCuenta + ", tipoCuenta=" + tipoCuenta
				+ ", titularCuenta=" + titularCuenta + ", correoTitular=" + correoTitular + ", telefonoTitular="
				+ telefonoTitular + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
