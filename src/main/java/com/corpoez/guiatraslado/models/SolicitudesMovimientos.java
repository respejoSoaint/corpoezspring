package com.corpoez.guiatraslado.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "solicitudesMovimientos")
public class SolicitudesMovimientos {
	
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMov;
	private int idSolicitud;
	private int idEstatus;
	private String observciones;
	private String createdAt;
	private int createdUser;
	public Long getIdMov() {
		return idMov;
	}
	public void setIdMov(Long idMov) {
		this.idMov = idMov;
	}
	public int getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(int idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public int getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(int idEstatus) {
		this.idEstatus = idEstatus;
	}
	public String getObservciones() {
		return observciones;
	}
	public void setObservciones(String observciones) {
		this.observciones = observciones;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public int getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(int createdUser) {
		this.createdUser = createdUser;
	}
	
	@Override
	public String toString() {
		return "SolicitudesMovimientos [idMov=" + idMov + ", idSolicitud=" + idSolicitud + ", idEstatus=" + idEstatus
				+ ", observciones=" + observciones + ", createdAt=" + createdAt + ", createdUser=" + createdUser + "]";
	}
	
	

}
