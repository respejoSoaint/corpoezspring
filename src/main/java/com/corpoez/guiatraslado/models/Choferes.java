package com.corpoez.guiatraslado.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="choferes")
public class Choferes {
	
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idChofer;
	private String cedula;
	private String nombres;
	private String apellidos;
	private String direccion;
	private String emailChofer;
	private String telefonoHabitacion;
	private String telefonoMovil;
	private String createdAt;
	private String updatedAt;
	public Long getIdChofer() {
		return idChofer;
	}
	public void setIdChofer(Long idChofer) {
		this.idChofer = idChofer;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEmailChofer() {
		return emailChofer;
	}
	public void setEmailChofer(String emailChofer) {
		this.emailChofer = emailChofer;
	}
	public String getTelefonoHabitacion() {
		return telefonoHabitacion;
	}
	public void setTelefonoHabitacion(String telefonoHabitacion) {
		this.telefonoHabitacion = telefonoHabitacion;
	}
	public String getTelefonoMovil() {
		return telefonoMovil;
	}
	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	@Override
	public String toString() {
		return "Choferes [idChofer=" + idChofer + ", cedula=" + cedula + ", nombres=" + nombres + ", apellidos="
				+ apellidos + ", direccion=" + direccion + ", emailChofer=" + emailChofer + ", telefonoHabitacion="
				+ telefonoHabitacion + ", telefonoMovil=" + telefonoMovil + ", createdAt=" + createdAt + ", updatedAt="
				+ updatedAt + "]";
	}
	
	

}
