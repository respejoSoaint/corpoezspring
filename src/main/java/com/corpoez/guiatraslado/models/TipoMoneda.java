package com.corpoez.guiatraslado.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="tipoMoneda")
public class TipoMoneda {
	
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTipoMoneda;
	private String descripcion;
	private String abreviacion;
	private String createdAt;
	private String updatedAt;
	public Long getIdTipoMoneda() {
		return idTipoMoneda;
	}
	public void setIdTipoMoneda(Long idTipoMoneda) {
		this.idTipoMoneda = idTipoMoneda;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAbreviacion() {
		return abreviacion;
	}
	public void setAbreviacion(String abreviacion) {
		this.abreviacion = abreviacion;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	@Override
	public String toString() {
		return "TipoMoneda [idTipoMoneda=" + idTipoMoneda + ", descripcion=" + descripcion + ", abreviacion="
				+ abreviacion + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
	}
	
	
	

}
