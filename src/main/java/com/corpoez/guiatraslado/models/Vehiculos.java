package com.corpoez.guiatraslado.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "vehiculos")
public class Vehiculos {
	
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idVehiculo;
	private String tipo;
	private String marca;
	private String color;
	private String placa;
	private String batea;
	private String condicion;
	private String capacidad;
	private int idTipoCarga;
	private String createdAt;
	private String updatedAt;
	public Long getIdVehiculo() {
		return idVehiculo;
	}
	public void setIdVehiculo(Long idVehiculo) {
		this.idVehiculo = idVehiculo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getBatea() {
		return batea;
	}
	public void setBatea(String batea) {
		this.batea = batea;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}
	public int getIdTipoCarga() {
		return idTipoCarga;
	}
	public void setIdTipoCarga(int idTipoCarga) {
		this.idTipoCarga = idTipoCarga;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
	@Override
	public String toString() {
		return "Vehiculos [idVehiculo=" + idVehiculo + ", tipo=" + tipo + ", marca=" + marca + ", color=" + color
				+ ", placa=" + placa + ", batea=" + batea + ", condicion=" + condicion + ", capacidad=" + capacidad
				+ ", idTipoCarga=" + idTipoCarga + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
	}
	
	

}
