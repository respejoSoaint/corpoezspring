package com.corpoez.guiatraslado.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.corpoez.guiatraslado.models.Bancos;
import com.corpoez.guiatraslado.services.IBancosService;

@RestController
@RequestMapping("/listas")
public class ListController {
	
	@Autowired
	private IBancosService bancosService;

	
	@GetMapping("/bancos")
	public List<Bancos> bancos() {
		return bancosService.findAll();
	}
}
